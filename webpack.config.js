var webpack = require('webpack'),
    MiniCssExtractPlugin = require('mini-css-extract-plugin'),    
    HtmlWebpackPlugin = require('html-webpack-plugin'),
    path = require('path');

const devMode = process.env.NODE_ENV !== 'production';

module.exports = {
  devServer: {
    port: 8000,
    historyApiFallback: true,
  },

  entry: {
    'script': './src/index.js',
    'style': './src/styles/index.scss',
  },

  module: {
    rules: [
      { test: /\.json$/, loader: 'json-loader'},
      { test: /\.js$/, exclude: /(node_modules|bower_components)\//, loader: 'babel-loader'},
      { test: /\.(ttf.*|eot.*|woff.*|ogg|mp3)$/, loader: 'file-loader'},
      { test: /.(png|jpe?g|gif|svg.*)$/, loader: 'file-loader'},
      {
        test: /\.(sa|sc|c)ss$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              hmr: process.env.NODE_ENV === 'development',
            },
          },
          'css-loader',
          'sass-loader',
        ],          
      },
      {
        test: /\.handlebars$/,
        loader: 'raw-loader',
      }
    ],
  },

  plugins: [
    new MiniCssExtractPlugin({
        filename: devMode ? '[name].css' : '[name].[hash].css',
        chunkFilename: devMode ? '[id].css' : '[id].[hash].css',
    }),
    new HtmlWebpackPlugin({
        title: 'Little Store',
        template: path.resolve(__dirname, '', 'src', 'html', 'index.ejs')        
    }),
    new webpack.DefinePlugin({
      CONFIG: JSON.stringify(require('config')),
    }),
  ],

  resolve: {
    alias: {
        handlebars: 'handlebars/dist/handlebars.min.js',
    },      
    modules: [
      path.resolve(__dirname, 'src'),
      path.resolve(__dirname, 'node_modules'),
    ],
  },

  output: {
    path: path.join(__dirname, 'dist'),
    filename: '[name].js',
  },
};