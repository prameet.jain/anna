require('jsdom-global')();
import $ from 'jquery';
var assert = require('assert');
const {expect} = require('chai');
import { compile } from 'handlebars'
import ChatWidget from './../src/components/chat-widget.js'

var chatWidgetTemplate = `<div class="anna__chat-widget__container">
<div class="anna__chat-widget__window anna__chat-widget__window-hide">
  <div class="anna__chat-widget__mobile-scroll">
    <div class="anna__chat-widget__header">
      <div class="anna__chat-widget__header-content">
        <div class="anna__chat-widget__header-title">Anna</div>
        <div class="anna__chat-widget__header-subtitle">Your shopping assitant at Little Store!</div>
      </div>
    </div>
    <div class="anna__chat-widget__messages">
    </div>
  </div>
  <form class="anna__chat-widget__input" id="anna__chat-widget__input">
    <div class="anna__chat-widget__input-message">
              <input class="anna__chat-widget__input-message-inputbox" value="" placeholder="Type a message...">
          </div>
  </form>
</div>
  <button class="anna__chat-widget__launcher anna__chat-widget__launcher__close">
      <img class="anna__chat-widget__launcher__open__img" src="src/assets/images/avataaars.svg">
      <img class="anna__chat-widget__launcher__close__img" src="src/assets/images/launcher-close.svg">
  </button>
</div>
`

var chatUserMsgTemplate = `<div class="anna__chat-widget__messages__user-input">
    <div class="anna__chat-widget__messages__message">
        <div class="anna__chat-widget__messages__message__user-message" style="background-color: rgb(124, 77, 255); color: rgb(255, 255, 255);">{{msg}}</div>
    </div>
</div>`

var chatBotMsgTemplate = `
<div class="pb-bot-response">
    <div class="anna__chat-widget__messages__message">
        <img class="pb-message__avatar" src="src/assets/images/avataaars.svg">
        <div class="anna__chat-widget__messages__message__bot-message">{{msg}}</div>
    </div>
</div>
`
describe('ChatWidget', function() {
  before(function () {
    $('body').html('<div class="anna__chat-widget"></div>');
    $('.anna__chat-widget').html(compile(chatWidgetTemplate)({}))
  });

  describe('constructor', function(){
    it('should have a random User Id', function(){
      expect(ChatWidget.randomUserId).not.empty;
    });
  
    it('should have a random User Id of length 10', function(){
      expect(ChatWidget.randomUserId).to.have.lengthOf(10);
    });
  
    it('should have a socket object initialized', function(){
      expect(ChatWidget.socket).not.empty;
    });
  });

  describe('sendMessage', function() {
    it('should throw error if message is empty', function(){
      expect(function() {
        ChatWidget.sendMessage();
      }).to.throw(Error);
    });

    it('should send only if message is non empty', function(){
      const response = ChatWidget.sendMessage('test');
      expect(response).to.equal(true);
    });
  });

  describe('appendUserMessage', function() {
    it('should throw error if message is empty', function(){
      expect(function() {
        ChatWidget.appendUserMessage();
      }).to.throw(Error);
    });

    it('should append only if message is non empty', function(){
      const msg = "test from user";
      $('.anna__chat-widget__messages').append(compile(chatUserMsgTemplate)({ msg }));

      expect($('.anna__chat-widget__messages__message__user-message').last().text()).equal('test from user');
    });
  });

  describe('appendBotMessage', function() {
    it('should throw error if message is empty', function(){
      expect(function() {
        ChatWidget.appendBotMessage();
      }).to.throw(Error);
    });

    it('should append only if message is non empty', function(){
      const msg = "test from bot";
      $('.anna__chat-widget__messages').append(compile(chatBotMsgTemplate)({ msg }));

      expect($('.anna__chat-widget__messages__message__bot-message').last().text()).equal('test from bot');
    });
  });
});