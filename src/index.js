import $ from 'jquery'
import Navigo from 'navigo';
import { compile } from 'handlebars'

import HomePage from './routes/home';
import NotFound from './routes/notFound';
import ProductsPage from './routes/products';

import AppTemplate from './html/app.handlebars';
import headerTemplate from './html/header.handlebars'
import footerTemplate from './html/footer.handlebars'

import ChatWidget from './components/chat-widget.js'

const router = new Navigo()

$(window).on('load', () => {
    $(document).on('click', '[data-path]', (e) => {
        e.preventDefault()
        const href = $(e.target).attr('href')

        if (process.env.DEBUG) {
            console.log(`Navigating to ${href}`)
        }

        router.navigate(href)
    })

    $('#app').html(compile(AppTemplate)({}))
    $('.anna__header').html(compile(headerTemplate)({}))
    $('.anna__footer').html(compile(footerTemplate)({}))
    
    ChatWidget.Init();

    router
    .on('/', HomePage)
    .on('/products', ProductsPage)
    .notFound(NotFound)
    .resolve()
})

require('./components/ls-navbar/ls-navbar')