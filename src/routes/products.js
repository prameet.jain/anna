import $ from 'jquery'
import { compile } from 'handlebars'
import template from './../html/products.handlebars'

export default (ctx, next) => {
    $('.anna__body').html(compile(template)({}))
}
