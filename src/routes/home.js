import $ from 'jquery'
import { compile } from 'handlebars'
import homeTemplate from './../html/home.handlebars'

export default (ctx, next) => {
    $('.anna__body').html(compile(homeTemplate)({}));
}
