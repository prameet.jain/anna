const template = document.createElement('template');
template.innerHTML = `
    <style>
      :host {
        display: flex;
        flex-wrap: wrap;
      }
    </style>
    <slot name="ls-nav-tab"></slot>
  `;
  
  export class LsNavBar extends HTMLElement {
    constructor() {
      super();
  
      this._onSlotChange = this._onSlotChange.bind(this);
  
      this.attachShadow({mode: 'open'});
      this.shadowRoot.appendChild(template.content.cloneNode(true));
  
      this._tabSlot = this.shadowRoot.querySelector('slot[name=ls-nav-tab]');
  
      this._tabSlot.addEventListener('slotchange', this._onSlotChange);
    }
  
    connectedCallback() {  
      this.addEventListener('click', this._onClick);
  
      if (!this.hasAttribute('role'))
        this.setAttribute('role', 'tablist');
  
      Promise.all([
        customElements.whenDefined('ls-nav-tab'),
      ])
      .then(_ => this._initTabs());
    }
  
    disconnectedCallback() {
      this.removeEventListener('click', this._onClick);
    }
  
    _onSlotChange() {
      this._initTabs();
    }
  
    _initTabs() {
      const tabs = this._allTabs();
  
      const selectedTab = tabs.find(tab => tab.selected) || tabs[0];  
      this._selectTab(selectedTab);
    }
  
  
    _allTabs() {
      return Array.from(this.querySelectorAll('ls-nav-tab'));
    }
    
    _prevTab() {
      const tabs = this._allTabs();
      let newIdx = tabs.findIndex(tab => tab.selected) - 1;
      return tabs[(newIdx + tabs.length) % tabs.length];
    }
  
    _firstTab() {
      return this._allTabs()[0];
    }
  
    _lastTab() {
      const tabs = this._allTabs();
      return tabs[tabs.length - 1];
    }
  
    _nextTab() {
      const tabs = this._allTabs();
      let newIdx = tabs.findIndex(tab => tab.selected) + 1;
      return tabs[newIdx % tabs.length];
    }
  
    reset() {
      const tabs = this._allTabs();
  
      tabs.forEach(tab => tab.selected = false);
    }
  
    _selectTab(newTab) {
      this.reset();

      newTab.selected = true;
      newTab.focus();
    }
  
    _onClick(event) {
        const sectectedTab = this.querySelector(`#${event.target.getAttribute('tab-ref')}`)
        this._selectTab(sectectedTab);
    }
  }
  
  let lsNavTabCounter = 0;
  
  export class LsNavTab extends HTMLElement {
    static get observedAttributes() {
      return ['selected'];
    }
  
    constructor() {
      super();
    }
  
    connectedCallback() {
      this.setAttribute('role', 'tab');
      if (!this.id) {
        const generatedId = `ls-nav-tab-generated-${lsNavTabCounter++}`;
        this.id = generatedId;
        this.querySelector('a').setAttribute('tab-ref', generatedId);
      }
  
      // Set a well-defined initial state.
      this.setAttribute('aria-selected', 'false');
      this._upgradeProperty('selected');
    }
  
    _upgradeProperty(prop) {
      if (this.hasOwnProperty(prop)) {
        let value = this[prop];
        delete this[prop];
        this[prop] = value;
      }
    }
  
    attributeChangedCallback() {
      const value = this.hasAttribute('selected');
      this.setAttribute('aria-selected', value);
    }
  
    set selected(value) {
      value = Boolean(value);
      if (value)
        this.setAttribute('selected', '');
      else
        this.removeAttribute('selected');
    }
  
    get selected() {
      return this.hasAttribute('selected');
    }
  }  

  window.customElements.define('ls-nav-bar', LsNavBar);
  window.customElements.define('ls-nav-tab', LsNavTab);
  