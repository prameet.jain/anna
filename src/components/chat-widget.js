import $ from 'jquery'
import { compile } from 'handlebars'
import chatWidgetTemplate from './../html/chat-widget.handlebars'
import chatUserMsgTemplate from './../html/chat-message-user.handlebars'
import chatBotMsgTemplate from './../html/chat-message-bot.handlebars'

import io from 'socket.io-client';

class ChatWidget {
    constructor() {
        this.randomUserId = this._getRandomId(10);
        this.socket = io('http://localhost:3600');
    }

    _getRandomId(len) {
        return [...Array(len)].map(i=>(~~(Math.random()*36)).toString(36)).join('');
    }

    sendMessage = (msg) => {
        if(!msg)
            throw new Error("missing message");

        this.socket.emit('send message', {
            room: this.randomUserId,
            message: msg
        });
        return true;
    }

    appendUserMessage = (msg) => {
        if(!msg)
            throw new Error("missing message");

        $('.anna__chat-widget__messages').append(compile(chatUserMsgTemplate)({ msg }));
        $('.anna__chat-widget__input-message-inputbox').val('');
    }

    appendBotMessage = (data) => {
        if(!data || !data.message)
            throw new Error("empty response");

        const msg = data.message;
        $('.anna__chat-widget__messages').append(compile(chatBotMsgTemplate)({ msg }));
        window.scrollTo(0, document.body.scrollHeight);
    }
    showChatWindow = () => {
        $('button.anna__chat-widget__launcher').addClass('anna__chat-widget__launcher__open');
        $('button.anna__chat-widget__launcher').removeClass('anna__chat-widget__launcher__close');

        $('.anna__chat-widget__window').addClass('anna__chat-widget__window-show');
        $('.anna__chat-widget__window').removeClass('anna__chat-widget__window-hide');        
    }
    hideChatWindow = () => {
        $('button.anna__chat-widget__launcher').addClass('anna__chat-widget__launcher__close');
        $('button.anna__chat-widget__launcher').removeClass('anna__chat-widget__launcher__open');

        $('.anna__chat-widget__window').addClass('anna__chat-widget__window-hide');
        $('.anna__chat-widget__window').removeClass('anna__chat-widget__window-show');
    }
    
    bindEvents = () => {
        $('.anna__chat-widget__launcher__open__img').click(this.showChatWindow);
        $('.anna__chat-widget__launcher__close__img').click(this.hideChatWindow);

        $('form#anna__chat-widget__input').submit((event) => {
            event.preventDefault();
            const msg = $('.anna__chat-widget__input-message-inputbox').val();
            if(msg){
                this.sendMessage(msg);
                this.appendUserMessage(msg);
            }
        });

        this.socket.emit('subscribe', this.randomUserId);

        this.socket.on('conversation private post', this.appendBotMessage);

        this.socket.on('connect', function(){
            console.log('socket connected')
        });
        this.socket.on('disconnect', function(){
            console.log('socket disconnected')
        });
    }

    render = () => {
        $('.anna__chat-widget').html(compile(chatWidgetTemplate)({}))
    }
    Init = () => {
        this.render();
        this.bindEvents();
    }

}
const chatwidget = new ChatWidget();
export default chatwidget;